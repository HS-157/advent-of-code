#!/usr/bin/env python3


def pairs():
    with open("./day4.txt", "r") as f:
        pairs = [l.replace("\n", "") for l in f.readlines()]
        p = []
        for pair in pairs:
            p1, p2 = pair.split(",")
            p11, p12 = p1.split("-")
            p21, p22 = p2.split("-")
            if p11 != p12:
                p1 = set(range(int(p11), int(p12) + 1))
            else:
                p1 = {int(p11)}
            if p21 != p22:
                p2 = set(range(int(p21), int(p22) + 1))
            else:
                p2 = {int(p22)}
            p.append([p1, p2])
        return p


def puzzle1():
    return sum([1 if s1.issubset(s2) or s2.issubset(s1) else 0 for s1, s2 in pairs()])


def puzzle2():
    return sum([1 for s1, s2 in pairs() if [True for e in s1 if e in s2]])


if __name__ == "__main__":
    print("> Day 4")
    print("Puzzle 1 answer : %s" % puzzle1())
    print("Puzzle 2 answer : %s" % puzzle2())
