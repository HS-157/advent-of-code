#!/usr/bin/env python3


def cargo():
    with open("./day5.txt", "r") as f:
        lines = [l.replace("\n", "") for l in f.readlines()]
        stack = lines[:8]
        moves = lines[10:]

        crates = {}
        for i in range(1, 10):
            crates[i] = []
            for s in reversed(stack):
                c = s[1 + 4 * (i - 1)]
                if c == " ":
                    break
                crates[i].append(c)
        return {"crates": crates, "moves": moves}


def puzzle1():
    c = cargo()
    moves = c["moves"]
    crates = c["crates"]
    for move in moves:
        s = move.split()
        loop = int(s[1])
        stack_in = int(s[3])
        stack_out = int(s[5])
        for i in range(loop):
            c = crates[stack_in].pop()
            crates[stack_out].append(c)
    return "".join([v[-1] for v in crates.values()])


def puzzle2():
    c = cargo()
    moves = c["moves"]
    crates = c["crates"]
    for move in moves:
        s = move.split()
        nb = int(s[1])
        stack_in = int(s[3])
        stack_out = int(s[5])
        c = crates[stack_in][-nb:]
        del crates[stack_in][-nb:]
        crates[stack_out] += c
    return "".join([v[-1] for v in crates.values()])


if __name__ == "__main__":
    print("> Day 5")
    print("Puzzle 1 answer : %s" % puzzle1())
    print("Puzzle 2 answer : %s" % puzzle2())
