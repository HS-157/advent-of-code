#!/usr/bin/env python3

from enum import Enum


class Janken(Enum):
    ROCK = 1
    PAPER = 2
    SCISSOR = 3


class Match(Enum):
    WIN = 6
    DRAW = 3
    LOST = 0


table = {
    "A X": {
        "p1": {"result": Match.DRAW, "hand": Janken.ROCK},
        "p2": {"result": Match.LOST, "hand": Janken.SCISSOR},
    },
    "B X": {
        "p1": {"result": Match.LOST, "hand": Janken.ROCK},
        "p2": {"result": Match.LOST, "hand": Janken.ROCK},
    },
    "C X": {
        "p1": {"result": Match.WIN, "hand": Janken.ROCK},
        "p2": {"result": Match.LOST, "hand": Janken.PAPER},
    },
    "A Y": {
        "p1": {"result": Match.WIN, "hand": Janken.PAPER},
        "p2": {"result": Match.DRAW, "hand": Janken.ROCK},
    },
    "B Y": {
        "p1": {"result": Match.DRAW, "hand": Janken.PAPER},
        "p2": {"result": Match.DRAW, "hand": Janken.PAPER},
    },
    "C Y": {
        "p1": {"result": Match.LOST, "hand": Janken.PAPER},
        "p2": {"result": Match.DRAW, "hand": Janken.SCISSOR},
    },
    "A Z": {
        "p1": {"result": Match.LOST, "hand": Janken.SCISSOR},
        "p2": {"result": Match.WIN, "hand": Janken.PAPER},
    },
    "B Z": {
        "p1": {"result": Match.WIN, "hand": Janken.SCISSOR},
        "p2": {"result": Match.WIN, "hand": Janken.SCISSOR},
    },
    "C Z": {
        "p1": {"result": Match.DRAW, "hand": Janken.SCISSOR},
        "p2": {"result": Match.WIN, "hand": Janken.ROCK},
    },
}


def janken():
    with open("./day2.txt", "r") as f:
        return [table[l.replace("\n", "")] for l in f.readlines()]


def puzzle1():
    return sum([r["p1"]["result"].value + r["p1"]["hand"].value for r in janken()])


def puzzle2():
    return sum([r["p2"]["result"].value + r["p2"]["hand"].value for r in janken()])


if __name__ == "__main__":
    print("> Day 2")
    print("Puzzle 1 answer : %s" % puzzle1())
    print("Puzzle 2 answer : %s" % puzzle2())
