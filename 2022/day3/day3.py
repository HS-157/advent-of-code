#!/usr/bin/env python3

import string


def backpacks():
    with open("./day3.txt", "r") as f:
        return [l.replace("\n", "") for l in f.readlines()]


def puzzle1():
    backs = backpacks()
    q = []
    for i in backs:
        l = len(i)
        l2 = int(l / 2)
        # print("%s - %s" % (i[:int(l2)], i[int(l2):]))
        double = set()
        for c in i[: int(l2)]:
            if c in i[int(l2) :]:
                double.add(c)
                # print(c)
        q.append(double.pop())
    # print(b)
    t = 0
    for a in q:
        t += string.ascii_letters.index(a) + 1
    return t


def puzzle2():
    a = backpacks()
    d = []
    for i in range(0, len(a), 3):
        x, y, z = a[i : i + 3]
        v = set()
        for j in x:
            if j in y and j in z:
                v.add(j)
        d.append(v.pop())
    t = 0
    for a in d:
        t += string.ascii_letters.index(a) + 1
    return t


if __name__ == "__main__":
    print("> Day 3")
    print("Puzzle 1 answer : %s" % puzzle1())
    print("Puzzle 2 answer : %s" % puzzle2())
