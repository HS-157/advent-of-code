#!/usr/bin/env python3


def signal(v):
    with open("./day6.txt", "r") as f:
        s = f.read().replace("\n", "")
    for i in range(len(s) - v):
        char = s[i : i + v]
        if len(char) == len(set(char)):
            return i + v


def puzzle1():
    return signal(4)


def puzzle2():
    return signal(14)


if __name__ == "__main__":
    print("> Day 6")
    print("Puzzle 1 answer : %s" % puzzle1())
    print("Puzzle 2 answer : %s" % puzzle2())
