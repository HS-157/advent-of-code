#!/usr/bin/env python3

def elves_calories():
    with open("./day1.txt", "r") as f:
        elves = f.read().split("\n\n")
    l = lambda x: 0 if "" == x else int(x)
    return [sum(map(l, elve.split("\n"))) for elve in elves]

def puzzle1():
    return max(elves_calories())

def puzzle2():
    return sum(sorted(elves_calories(), reverse=True)[:3])

if __name__ == '__main__':
    print("> Day 1")
    print("Puzzle 1 answer : %s" % puzzle1())
    print("Puzzle 2 answer : %s" % puzzle2())
