use regex::Regex;
use std::fs;

fn puzzle1() {
    let blue_re = Regex::new(r"(\d+) blue").unwrap();
    let red_re = Regex::new(r"(\d+) red").unwrap();
    let green_re = Regex::new(r"(\d+) green").unwrap();

    let mut game_ids: Vec<i32> = vec![];
    let mut impossible: Vec<i32> = vec![];

    let read = fs::read_to_string("./day2.txt").expect("Don't find the file");
    let lines = read.lines();
    for line in lines {
        let game_id = line.split(":").collect::<Vec<_>>()[0]
            .split_whitespace()
            .collect::<Vec<_>>()[1]
            .to_string()
            .parse::<i32>()
            .unwrap();
        game_ids.push(game_id);
        // println!("ID : {}", game_id);

        let pulls = line.split(":").collect::<Vec<_>>()[1]
            .split(";")
            .collect::<Vec<_>>();

        for pull in pulls {
            // println!("Pull : {}", pull);

            let blue_re = blue_re.captures(pull);
            let blue = if blue_re.is_some() {
                blue_re
                    .unwrap()
                    .get(1)
                    .map_or(0, |m| m.as_str().parse::<i32>().unwrap())
            } else {
                0i32
            };

            let red_re = red_re.captures(pull);
            let red = if red_re.is_some() {
                red_re
                    .unwrap()
                    .get(1)
                    .map_or(0, |m| m.as_str().parse::<i32>().unwrap())
            } else {
                0i32
            };

            let green_re = green_re.captures(pull);
            let green = if green_re.is_some() {
                green_re
                    .unwrap()
                    .get(1)
                    .map_or(0, |m| m.as_str().parse::<i32>().unwrap())
            } else {
                0i32
            };

            //println!("Blue : {:?} - Red : {} - Green : {}", blue, red, green);

            if blue > 14 || red > 12 || green > 13 {
                impossible.push(game_id);
            };
        }
    }

    let mut possible: Vec<i32> = vec![];
    for game in game_ids {
        if !impossible.contains(&game) {
            possible.push(game.clone())
        }
    }
    let sum: i32 = possible.iter().sum();
    println!("Puzzle 1 : {:?}", sum)
}

fn puzzle2() {
    let blue_re = Regex::new(r"(\d+) blue").unwrap();
    let red_re = Regex::new(r"(\d+) red").unwrap();
    let green_re = Regex::new(r"(\d+) green").unwrap();

    let mut game_ids: Vec<i32> = vec![];

    let mut cubes: Vec<i32> = vec![];

    let read = fs::read_to_string("./day2.txt").expect("Don't find the file");
    let lines = read.lines();
    for line in lines {
        let game_id = line.split(":").collect::<Vec<_>>()[0]
            .split_whitespace()
            .collect::<Vec<_>>()[1]
            .to_string()
            .parse::<i32>()
            .unwrap();
        game_ids.push(game_id);
        // println!("ID : {}", game_id);

        let pulls = line.split(":").collect::<Vec<_>>()[1]
            .split(";")
            .collect::<Vec<_>>();

        let mut vec_red: Vec<i32> = vec![];
        let mut vec_green: Vec<i32> = vec![];
        let mut vec_blue: Vec<i32> = vec![];

        for pull in pulls {
            // println!("Pull : {}", pull);

            let blue_re = blue_re.captures(pull);
            let blue = if blue_re.is_some() {
                blue_re
                    .unwrap()
                    .get(1)
                    .map_or(0, |m| m.as_str().parse::<i32>().unwrap())
            } else {
                0i32
            };

            let red_re = red_re.captures(pull);
            let red = if red_re.is_some() {
                red_re
                    .unwrap()
                    .get(1)
                    .map_or(0, |m| m.as_str().parse::<i32>().unwrap())
            } else {
                0i32
            };

            let green_re = green_re.captures(pull);
            let green = if green_re.is_some() {
                green_re
                    .unwrap()
                    .get(1)
                    .map_or(0, |m| m.as_str().parse::<i32>().unwrap())
            } else {
                0i32
            };

            // println!("Blue : {:?} - Red : {} - Green : {}", blue, red, green);
            vec_red.push(red);
            vec_green.push(green);
            vec_blue.push(blue)
        }
        let max_red = vec_red.iter().max().unwrap();
        let max_green = vec_green.iter().max().unwrap();
        let max_blue = vec_blue.iter().max().unwrap();

        // println!("Max blue : {} - {:?}", max_blue, vec_blue);
        // println!("Max red : {} - {:?}", max_red, vec_red);
        // println!("Max green : {} - {:?}", max_green, vec_green);

        let multi = max_red * max_green * max_blue;

        // println!("Multi : {}", multi);
        cubes.push(multi)
    }

    let sum: i32 = cubes.iter().sum();
    println!("Puzzle 2 : {:?}", sum)
}

fn main() {
    println!("> Day 1");
    puzzle1();
    puzzle2();
}
