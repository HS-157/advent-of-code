use core::fmt::Display;
use std::collections::HashSet;
use std::fs;

#[derive(Hash, Eq, PartialEq, Debug, Clone)]
struct Pos {
    x: usize,
    y: usize,
}

impl Pos {
    fn new(x: usize, y: usize) -> Self {
        Self { x, y }
    }
}

impl Display for Pos {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

#[derive(Debug)]
struct Symbol {
    symbol: String,
    pos: Pos,
}

impl Symbol {
    fn new(symbol: String, pos: Pos) -> Self {
        Self { symbol, pos }
    }
}

impl Display for Symbol {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} - {}", self.symbol, self.pos)
    }
}

#[derive(Clone, Eq, Hash, PartialEq, Debug)]
struct Number {
    number: usize,
    pos: Vec<Pos>,
}

impl Number {
    fn new(number: usize, pos: Vec<Pos>) -> Self {
        Self { number, pos }
    }
}

impl Display for Number {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // write!(f, "{} - {:?}", self.number, self.pos)
        write!(f, "{}", self.number)
    }
}

fn puzzle1() -> (Vec<Symbol>, HashSet<Number>) {
    let read = fs::read_to_string("./day3.txt").expect("Don't find the file");
    let lines = read.lines();

    let mut symbols: Vec<Symbol> = vec![];
    // let mut numbers: Vec<Number> = vec![];
    let mut numbers: HashSet<Number> = vec![].into_iter().collect();

    for (i, line) in lines.enumerate() {
        let mut number_vec: Vec<Pos> = vec![];
        let mut number_str = String::new();

        // print!("Line {} : ", i);
        for (j, c) in line.chars().enumerate() {
            let pos = Pos::new(i, j);

            match c {
                c if !c.is_numeric() && c != '.' => {
                    let symbol = Symbol::new(c.to_string(), pos);
                    symbols.push(symbol);
                    // print!("{}", c);
                    if !number_vec.is_empty() && !number_str.is_empty() {
                        let ns = number_str.parse::<usize>().unwrap();
                        let n = Number::new(ns, number_vec.clone());

                        numbers.insert(n);

                        number_vec = vec![];
                        number_str = String::new();
                    }
                }
                c if c.is_numeric() => {
                    number_vec.push(pos);
                    number_str.push_str(&c.to_string());
                    // print!("{}", c);
                }
                _ => {
                    // print!("{}", c);
                    if !number_vec.is_empty() && !number_str.is_empty() {
                        let ns = number_str.parse::<usize>().unwrap();
                        let n = Number::new(ns, number_vec.clone());

                        numbers.insert(n);

                        number_vec = vec![];
                        number_str = String::new();
                    }
                }
            }
        }
        if !number_vec.is_empty() && !number_str.is_empty() {
            let ns = number_str.parse::<usize>().unwrap();
            let n = Number::new(ns, number_vec.clone());

            numbers.insert(n);
        }
        // println!()
    }
    // print!("Symbols : ");
    // for s in &symbols {
    //     print!("'{}' ", s);
    // }
    // println!();
    // println!("Numbers : {}", numbers.len());
    // for n in &numbers {
    //     println!("{} ", n);
    // }

    let mut next: HashSet<Number> = vec![].into_iter().collect();
    for symbol in &symbols {
        // print!("S : {} {} - ", symbol.symbol, symbol.pos);
        let x = symbol.pos.x;
        let y = symbol.pos.y;
        let all_pos: Vec<Pos> = vec![
            Pos::new(x + 1, y),
            Pos::new(x, y + 1),
            Pos::new(x - 1, y),
            Pos::new(x, y - 1),
            Pos::new(x + 1, y + 1),
            Pos::new(x - 1, y - 1),
            Pos::new(x - 1, y + 1),
            Pos::new(x + 1, y - 1),
        ];
        // print!("{:?}", all_pos);
        // println!();
        for number in &numbers {
            for pos in &all_pos {
                if number.pos.contains(&pos) {
                    next.insert(number.clone());
                    break;
                };
            }
        }
    }
    // println!("Numbers : {:?}", numbers.len());
    // println!("Number next to Symbol : {:?}", next.len());
    // let diff = numbers.difference(&next).collect::<Vec<_>>();
    // println!("Not next to Symbol : {:?}", diff.len());
    let sum: usize = next.iter().map(|x| x.number).sum();
    // for n in diff {println!{"{} - {:?}", n.number, n.pos}}
    println!("Puzzle 1 : {:?}", sum);

    return (symbols, numbers);
}

fn puzzle2(symbols: Vec<Symbol>, numbers: HashSet<Number>) {
    let mut next: HashSet<Vec<_>> = vec![].into_iter().collect();
    for symbol in &symbols {
        if symbol.symbol != "*" {continue}
        let mut s_number: Vec<Number> = vec![];
        // print!("S : {} {} - ", symbol.symbol, symbol.pos);
        let x = symbol.pos.x;
        let y = symbol.pos.y;
        let all_pos: Vec<Pos> = vec![
            Pos::new(x + 1, y),
            Pos::new(x, y + 1),
            Pos::new(x - 1, y),
            Pos::new(x, y - 1),
            Pos::new(x + 1, y + 1),
            Pos::new(x - 1, y - 1),
            Pos::new(x - 1, y + 1),
            Pos::new(x + 1, y - 1),
        ];
        // print!("{:?}", all_pos);
        // println!();
        for number in &numbers {
            for pos in &all_pos {
                if number.pos.contains(&pos) {
                    s_number.push(number.clone());
                    break;
                };
            }
        }

        next.insert(s_number);
    }

    let mut total: Vec<usize> = vec![];
    for n in next{
        if n.len() != 2 {
            // println!("Len : {}", numbers.len());
            continue}
    else {
        let a = &n[0];
        let b = &n[1];
        total.push(a.number * b.number);
        }
        }
    let sum: usize = total.iter().sum();
    println!("Puzzle 2 : {:?}", sum);


}
fn main() {
    let (symbols, numbers) = puzzle1();
    puzzle2(symbols, numbers);
}
