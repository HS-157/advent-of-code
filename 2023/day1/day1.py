import string

digits_letter = ["one", "two","three", "four", "five", "six", "seven", "eight", "nine"]
start = [digit[0] for digit in digits_letter]
dict_digit = {
        "one": 1,
        "two": 2,
        "three": 3,
        "four": 4,
        "five": 5,
        "six": 6,
        "seven": 7,
        "eight": 8,
        "nine": 9
        }

def load():
    with open("./day1.txt") as f:
        return [l.replace("\n", "") for l in f.readlines()]

def puzzle1():
    values = load()
    calibration = []
    
    for value in values:
        first = None
        last = None
        for char in value:
            if char in string.digits:
                if not first:
                    first = char
                    last = first
                else:
                    last = char
        v = f"{first}{last}"
        calibration.append(int(v))

    return sum(calibration)

def puzzle2():
    values = load()
    calibration = []

    for value in values:
        first = None
        last = None
        for i in range(len(value)):
            char = value[i]
            if char in string.digits:
                if not first:
                    first = char
                    last = first
                else:
                    last = char
            elif char in start:
                for j in range(6):
                    word = value[i:i+j]
                    if word in digits_letter:
                        if not first:
                            first = dict_digit[word]
                            last = first
                            break
                        else:
                            last = dict_digit[word]
                            break
        v = f"{first}{last}"
        calibration.append(int(v))

    return sum(calibration)



if __name__ == "__main__":
    print("> Day 1")
    print("Puzzle 1 answer : %s" % puzzle1())
    print("Puzzle 2 answer : %s" % puzzle2())
